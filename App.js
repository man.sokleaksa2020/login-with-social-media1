import './App.css';
import "bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import { Container } from "react-bootstrap";
import LoginWithFacebook from './views/LoginWithFacebook'
import LoginWithGoogle from './views/LoginWithGoogle';

function App() {
  return (
    <div className="App">
      <BrowserRouter>
      <Container>
        <Switch>
          { <Route path="/" exact component={LoginWithFacebook} /> }
          {/* <Route path="/" exact component={LoginWithGoogle} /> */}
        </Switch>
      </Container>
      </BrowserRouter>
    </div>
  );
}

export default App;
