import React from 'react'
import { GoogleLogin } from 'react-google-login';
const responseGoogle = (response) => {
    console.log(response);
  }
export default function LoginWithGoogle() {
    return (
        <div>
            <GoogleLogin
    clientId="468831470718-3g33udt3hsv4jmf5222m2ohhui5icptm.apps.googleusercontent.com"
    buttonText="Login"
    onSuccess={responseGoogle}
    onFailure={responseGoogle}
    cookiePolicy={'single_host_origin'}
  />, 
        </div>
    )
}
