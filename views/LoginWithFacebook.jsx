import React, { Component } from 'react';
import FacebookLogin from 'react-facebook-login';


function LoginWithFacebook() {

  const conponentClicked = data => {
    console.log("data",data);
  };

    const responseFacebook = (response) => {
      
        console.log(response);
      };


    return (
      <div className="App">
        <FacebookLogin
            appId="338226617743415"
            autoLoad={true}
            fields="name,email,picture"
            onClick={conponentClicked}
            callback={responseFacebook}
            
            /* cssClass="my-facebook-button-class"
            icon="fa-facebook" */
        />,
      </div>
    );
  }
  
  export default LoginWithFacebook;